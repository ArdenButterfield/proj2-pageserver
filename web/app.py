from flask import Flask
from flask import render_template
from flask import request

app = Flask(__name__, template_folder="templates")

# Based on the method used here: https://stackoverflow.com/questions/13678397/python-flask-default-route-possible
@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def handle_all(path):
    if ("~" in path) or ("//" in path) or (".." in path):
        return render_template('403.html'), 403
    try:
        return render_template(f"./{path}")
    except FileNotFoundError:
        return render_template('404.html'), 404

@app.errorhandler(500)
# If template is not found flask raises a 500 error, but we want a 404 error.
def error_404(e):
    return render_template('404.html'), 404

@app.errorhandler(403)
def error_403(e):
    return render_template('403.html'), 403

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')